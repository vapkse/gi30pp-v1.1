PLOT REPORT
-----------

Report File:        Z:\Serge\My Documents\Electronique\GI30PP V1.1\Designspark\Transformer Entry LL1592\Transformer Entry LL1592 PCB (PCB - PLOT REPORT).txt
Report Written:     Tuesday, November 29, 2016
Design Path:        Z:\Serge\My Documents\Electronique\GI30PP V1.1\Designspark\Transformer Entry LL1592\Transformer Entry LL1592 PCB.pcb
Design Title:       
Created:            23.04.2016 12:29:25
Last Saved:         27.11.2016 23:33:21
Editing Time:       45 min
Units:              mm (precision 1)


Device Settings
===============


Gerber Settings
===============

    Leading zero suppression.
    G01 assumed throughout.
    Line termination <*> <CR> <LF>.
    3.5 format absolute inches.
    Format commands defined in Gerber file.
    Aperture table defined in Gerber file.
    Hardware arcs allowed.


Drill Settings
==============

    Excellon Format 1    Format 3.5 absolute in mm.
    No zero suppression.


Plots Output
============


Gerber Output
=============

Z:\Serge\My Documents\Electronique\GI30PP V1.1\Designspark\Transformer Entry LL1592\Transformer Entry LL1592 PCB - Top Silkscreen.gbr

    Layers:
    ------------------
    Top Silkscreen

    D-Code Shape  (thou)
    -----------------------
    D10    Round 5.00
    D12    Round 10.00

Gerber Output
=============

Z:\Serge\My Documents\Electronique\GI30PP V1.1\Designspark\Transformer Entry LL1592\Transformer Entry LL1592 PCB - Bottom Copper.gbr

    Layers:
    -----------------
    Bottom Copper

    D-Code Shape  (thou)
    -----------------------
    D10    Round 5.00
    D12    Round 10.00
    D13    Round 39.37
    D17    Square 100.00
    D73    Round 157.48
    D74    Round 60.00
    D75    Round 23.62

Gerber Output
=============

Z:\Serge\My Documents\Electronique\GI30PP V1.1\Designspark\Transformer Entry LL1592\Transformer Entry LL1592 PCB - Bottom Copper (Resist).gbr

    Layers:
    -----------------
    Bottom Copper

    D-Code Shape  (thou)
    -----------------------
    D76    Round 167.48
    D77    Round 135.98
    D78    Round 70.00
    D79    Square 110.00

Gerber Output
=============

Z:\Serge\My Documents\Electronique\GI30PP V1.1\Designspark\Transformer Entry LL1592\Transformer Entry LL1592 PCB - Bottom Copper (Paste).gbr

    Layers:
    -----------------
    Bottom Copper

    D-Code Shape  (thou)
    -----------------------

Gerber Output
=============

Z:\Serge\My Documents\Electronique\GI30PP V1.1\Designspark\Transformer Entry LL1592\Transformer Entry LL1592 PCB - Bottom Documentation.gbr

    Layers:
    ------------------------
    Bottom Documentation

    D-Code Shape  (thou)
    -----------------------
    D11    Round 9.84

NC Drill Output
===============

Z:\Serge\My Documents\Electronique\GI30PP V1.1\Designspark\Transformer Entry LL1592\Transformer Entry LL1592 PCB - Drill Data - [Through Hole].drl

    Output:
    ---------------------------
    Plated Round Drill Holes
    Plated Slots
    Plated Board Outlines

    Tool  Size         Count   ID
    ------------------------------------
    T001  000.03200 in 8     F
    T002  000.04000 in 9     H
    T003  000.07874 in 9     O
    ------------------------------------
    Total              26
    ------------------------------------

Z:\Serge\My Documents\Electronique\GI30PP V1.1\Designspark\Transformer Entry LL1592\Transformer Entry LL1592 PCB - Drill Data - [Through Hole] (Unplated).drl

    Output:
    -----------------------------
    Unplated Round Drill Holes
    Unplated Slots
    Unplated Board Outlines

    Tool  Size         Count   ID
    ------------------------------------
    T001  000.02500 in 1
    T002  000.12598 in 3     V
    ------------------------------------
    Total              4
    ------------------------------------


Gerber Output
=============

Z:\Serge\My Documents\Electronique\GI30PP V1.1\Designspark\Transformer Entry LL1592\Transformer Entry LL1592 PCB - Drill Ident Drawing - [Through Hole].gbr

    Layers:
    ------------------
    [Through Hole]


Drill Ident Drawing
===================

    Drill Plated Size Shape ID
    -----------------------------------
    32    Y      100  Round F
    40    Y      100  Round H
    79    Y      100  Round O
    126          100  Round V

    D-Code Shape  (thou)
    -----------------------
    D10    Round 5.00
    D14    Round 100.00

End Of Report.
