Design Rule Check Report
------------------------

Report File:        Z:\Serge\My Documents\Electronique\GI30PP V1.1\Designspark\Transformer Entry LL1592\Transformer Entry LL1592 PCB (PCB - Design Rule Check Report).txt
Report Written:     Tuesday, November 29, 2016
Design Path:        Z:\Serge\My Documents\Electronique\GI30PP V1.1\Designspark\Transformer Entry LL1592\Transformer Entry LL1592 PCB.pcb
Design Title:       
Created:            23.04.2016 12:29:25
Last Saved:         27.11.2016 23:33:21
Editing Time:       45 min
Units:              mm (precision 1)


Results
=======


No errors found


Settings
========


Spacings

=========

Tracks        Yes
Pads and Vias Yes
Shapes        No
Text          Yes
Board         Yes
Drills        Yes
Components    No


Manufacturing

==============

Drill Breakout                  No
Drill Backoff                   No
Silkscreen Overlap              No
Copper Text In Board            No
Min Track Width                 No
Min Annular Ring                No
Min Paste Size                  No
Vias In Pads                    No
Unplated Vias                   No
Unplated Pads With Inner Tracks No


Nets

=====

Net Completion               Yes
Dangling Tracks              Yes
Net Track Length Differences No



End Of Report.
